## [1.0.1](https://gitlab.com/just-ci/tools/security/cats-runner/compare/v1.0.0...v1.0.1) (2022-09-20)


### Bug Fixes

* show run details ([510a6eb](https://gitlab.com/just-ci/tools/security/cats-runner/commit/510a6eb449af7725a8aaaa3419bcafa3b1a5fd45))

# 1.0.0 (2022-09-05)


### Features

* cats in ci ([e63b1d0](https://gitlab.com/just-ci/tools/security/cats-runner/commit/e63b1d0b6e00f11c266bf8a7ebc33fac204f5a24))
