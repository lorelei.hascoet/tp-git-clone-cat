# CATS Runner v1.0.1

Runs the [CATS fuzzer](https://github.com/Endava/cats) in CI against a sidecar
container image.
